package com.dicoding.submissiondicoding2.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dicoding.submissiondicoding2.Adapter.MovieCardAdapter
import com.dicoding.submissiondicoding2.R
import com.dicoding.submissiondicoding2.model.TvViewModel


class TvFragment : Fragment() {

    private lateinit var adapter: MovieCardAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var tvViewModel: TvViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_tv, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressBar)
        val rvTv = view.findViewById<View>(R.id.tv_list) as RecyclerView

        rvTv.setHasFixedSize(true)
        adapter = MovieCardAdapter()
        adapter.notifyDataSetChanged()
        rvTv.layoutManager = LinearLayoutManager(context)
        rvTv.adapter = adapter

        tvViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(TvViewModel::class.java)
        tvViewModel.getTV().observe(viewLifecycleOwner, Observer { moviesItem ->
            if (moviesItem != null) {
                adapter.setData(moviesItem)
            }
            showLoading(false)
        })
        tvViewModel.setTV()
        showLoading(true)
    }

    private fun showLoading(state: Boolean) {
        if (state) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }
}
