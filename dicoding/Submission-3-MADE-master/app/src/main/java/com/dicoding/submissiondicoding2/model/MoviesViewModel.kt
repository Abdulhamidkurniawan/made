package com.dicoding.submissiondicoding2.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class MoviesViewModel: ViewModel() {
    companion object {
        private const val API_KEY = "23c0103173152795340f520505c7a2a7"
    }

    val listMovie = MutableLiveData<ArrayList<Data>>()

    internal fun setMovie(){
        val client = AsyncHttpClient()
        val listItems = ArrayList<Data>()
        val url = "https://api.themoviedb.org/3/discover/movie?api_key=$API_KEY&language=en-US"
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<Header>, responseBody: ByteArray) {
                try {
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)
                    val list = responseObject.getJSONArray("results")
                    for (i in 0 until list.length()) {
                        val movie = list.getJSONObject(i)
                        val movieItems = Data(
                            title = movie.getString("title"),
                            overview = movie.getString("overview"),
                            poster_path = movie.getInt("poster_path"),
                            genre_ids = movie.getString("genre_ids"),
                            vote_average = movie.getString("vote_average"),
                            release_date = movie.getString("release_date")
                        )
                        listItems.add(movieItems)
                    }
                    listMovie.postValue(listItems)
                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }
            override fun onFailure(statusCode: Int, headers: Array<Header>, responseBody: ByteArray, error: Throwable) {
                Log.d("onFailure", error.message.toString())
            }
        })
    }

    internal fun getMovie(): LiveData<ArrayList<Data>> {
        return listMovie
    }
}

