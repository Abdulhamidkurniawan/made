package com.dicoding.submissiondicoding2.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.dicoding.submissiondicoding2.model.Data
import com.dicoding.submissiondicoding2.R
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovie : AppCompatActivity() {
    private lateinit var progressBar: ProgressBar
    companion object {
        const val EXTRA_MOVIE = "extra_movie"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)
        val actionBar = supportActionBar
        actionBar?.title = title
        actionBar?.setDisplayHomeAsUpEnabled(true)
        progressBar = findViewById(R.id.progressBar)

        val vtName: TextView = findViewById(R.id.tv_name)
        val vtGenre: TextView = findViewById(R.id.tv_genre)
        val vtYears: TextView = findViewById(R.id.tv_years)
        val vtRate: TextView = findViewById(R.id.tv_rate)
        val vtDesc: TextView = findViewById(R.id.tv_description)

        val movie = intent.getParcelableExtra<Data>(EXTRA_MOVIE)

        vtName.text = movie?.title
        vtDesc.text = movie?.overview
        Glide.with(this)
            .load(movie?.poster_path)
            .into(imgPhoto)
        vtGenre.text = movie?.genre_ids
        vtRate.text = movie?.vote_average
        vtYears.text = movie?.release_date
    }
}
