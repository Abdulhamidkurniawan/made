package com.dicoding.submissiondicoding2.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dicoding.submissiondicoding2.Activity.DetailMovie
import com.dicoding.submissiondicoding2.model.Data
import com.dicoding.submissiondicoding2.R
import kotlinx.android.synthetic.main.activity_detail_movie.view.*
import kotlinx.android.synthetic.main.item_cardview.view.*



class MovieCardAdapter: RecyclerView.Adapter<MovieCardAdapter.MoviesViewHolder>() {

    private val cardMovie = ArrayList<Data>()

    fun setData(items: ArrayList<Data>) {
        cardMovie.clear()
        cardMovie.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MoviesViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_cardview, viewGroup, false)
        return MoviesViewHolder(view)
    }

    override fun getItemCount(): Int = cardMovie.size

    override fun onBindViewHolder(moviesViewHolder: MoviesViewHolder, position: Int) {
        moviesViewHolder.bind(cardMovie[position])
    }

    inner class MoviesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind (dataItems: Data){
            with(itemView){
                tv_name.text=dataItems.title
                tv_description.text=dataItems.overview
                tv_genre.text=dataItems.genre_ids
                tv_years.text=dataItems.release_date
                tv_rate.text=dataItems.vote_average

                Glide.with(itemView.context)
                    .load("https://image.tmdb.org/t/p/w185/${dataItems.poster_path}")
                    .placeholder(R.color.colorAccent)
                    .dontAnimate()
                    .into(img_photo)

                itemView.setOnClickListener {
                    val moving = Intent(itemView.context, DetailMovie::class.java)
                    val moviess = ArrayList<Data>()
                    val rcMovie = Data(
                        title = dataItems.title,
                        overview = dataItems.overview,
                        vote_average = dataItems.vote_average,
                        release_date = dataItems.release_date,
                        genre_ids = dataItems.genre_ids,
                        poster_path = dataItems.poster_path
                    )
                    moviess.add(rcMovie)
                    moving.putParcelableArrayListExtra(DetailMovie.EXTRA_MOVIE, moviess)
                    itemView.context.startActivity(moving)
                }
            }
        }
    }
}