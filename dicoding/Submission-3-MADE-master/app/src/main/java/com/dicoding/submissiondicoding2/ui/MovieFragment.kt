package com.dicoding.submissiondicoding2.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dicoding.submissiondicoding2.Adapter.MovieCardAdapter
import com.dicoding.submissiondicoding2.R
import com.dicoding.submissiondicoding2.model.Data
import com.dicoding.submissiondicoding2.model.MoviesViewModel


class MovieFragment: Fragment() {
    private lateinit var adapter: MovieCardAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var moviesViewModel: MoviesViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_movie, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rvMovie = view.findViewById<View>(R.id.lv_list) as RecyclerView

        rvMovie.setHasFixedSize(true)
        adapter = MovieCardAdapter()
        adapter.notifyDataSetChanged()
        rvMovie.layoutManager = LinearLayoutManager(context)
        rvMovie.adapter = adapter
        progressBar = view.findViewById(R.id.progressBar)

        moviesViewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MoviesViewModel::class.java)
        moviesViewModel.getMovie().observe(viewLifecycleOwner, Observer { moviesItem ->
            if (moviesItem != null) {
                adapter.setData(moviesItem)
            }
//            showLoading(false)
        })
        moviesViewModel.setMovie()
//        showLoading(true)
    }


    private fun showLoading(state: Boolean) {
        if (state) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }
}



