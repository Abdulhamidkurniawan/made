package com.dicoding.submissiondicoding2.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class TvViewModel: ViewModel() {
    companion object {
        private const val API_KEY = "23c0103173152795340f520505c7a2a7"
    }

    val listMovie = MutableLiveData<ArrayList<Data>>()

    internal fun setTV(){
        val client = AsyncHttpClient()
        val listItems = ArrayList<Data>()
        val url = "https://api.themoviedb.org/3/discover/tv?api_key=$API_KEY&language=en-US"
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<Header>, responseBody: ByteArray) {
                try {
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)
                    val list = responseObject.getJSONArray("results")
                    for (i in 0 until list.length()) {
                        val tv = list.getJSONObject(i)
                        val tvItems = Data(
                            title = tv.getString("title"),
                            overview = tv.getString("overview"),
                            poster_path = tv.getInt("poster_path"),
                            genre_ids = tv.getString("genre_ids"),
                            vote_average = tv.getString("vote_average"),
                            release_date = tv.getString("release_date")
                        )
                        listItems.add(tvItems)
                    }
                    listMovie.postValue(listItems)
                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }
            override fun onFailure(statusCode: Int, headers: Array<Header>, responseBody: ByteArray, error: Throwable) {
                Log.d("onFailure", error.message.toString())
            }
        })
    }

    internal fun getTV(): LiveData<ArrayList<Data>> {
        return listMovie
    }
}