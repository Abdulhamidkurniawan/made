package com.dicoding.submissiondicoding2.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    var title: String?,
    val poster_path: Int,
    val overview: String?,
    val genre_ids: String?,
    var vote_average: String,
    val release_date: String?
): Parcelable
