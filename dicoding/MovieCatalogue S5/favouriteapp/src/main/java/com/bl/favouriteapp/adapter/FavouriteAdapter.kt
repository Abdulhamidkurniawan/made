package com.bl.favouriteapp.adapter

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.bl.favouriteapp.CustomOnItemClickListener
import com.bl.favouriteapp.R
import com.bl.favouriteapp.activity.DetailFavActivity
import com.bl.favouriteapp.db.DatabaseContract
import com.bl.favouriteapp.entity.Favourite
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_favourite.view.*


class FavouriteAdapter (private val activity: Activity) : RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder>() {
//    private lateinit var favouriteHelper: FavouriteHelper
    lateinit var uriWithId: Uri
    lateinit var favourite: Favourite

    var listFavourites = ArrayList<Favourite>()
        set(listFavourites) {
            if (listFavourites.size > 0) {
                this.listFavourites.clear()
            }
            this.listFavourites.addAll(listFavourites)
            notifyDataSetChanged()
        }

    fun addItem(favourite: Favourite) {
        this.listFavourites.add(favourite)
        notifyItemInserted(this.listFavourites.size - 1)
    }

    fun updateItem(position: Int, favourite: Favourite) {
        this.listFavourites[position] = favourite
        notifyItemChanged(position, favourite)
    }

    fun removeItem(position: Int) {
        this.listFavourites.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, this.listFavourites.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favourite, parent, false)
        lateinit var uriWithId: Uri
        return FavouriteViewHolder(view)
    }

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        val context = holder.itemView.context
        val dialogTitle: String
        val dialogMessage: String
        val favourite = Favourite()
        uriWithId = Uri.parse(DatabaseContract.FavouriteColumns.CONTENT_URI.toString() + "/" + favourite?.id)

        dialogMessage = "Apakah anda yakin ingin menghapus item ini?"
        dialogTitle = "Hapus Favourite"
        holder.bind(listFavourites[position])

//        holder.delFavorite.setOnClickListener {
//        val alertDialogBuilder = AlertDialog.Builder(context)
//        alertDialogBuilder.setTitle(dialogTitle)
//        alertDialogBuilder
//            .setMessage(dialogMessage)
//            .setCancelable(false)
//            .setPositiveButton("Ya") { dialog, id ->
//                val result = context.contentResolver.delete(uriWithId, null, null)
//                if (result > 0) {
//                    Toast.makeText(holder.itemView.context, "Hapus " + listFavourites[position].title, Toast.LENGTH_SHORT).show()
//                    removeItem(position)
//                } else {
//                    Toast.makeText(holder.itemView.context, "Gagal menghapus data", Toast.LENGTH_SHORT).show()
//                }
//                }
//
//            .setNegativeButton("Tidak") { dialog, id -> dialog.cancel() }
//            val alertDialog = alertDialogBuilder.create()
//            alertDialog.show()
////            favouriteHelper.open()
//
//        }
    }

    override fun getItemCount(): Int = this.listFavourites.size

    inner class FavouriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var delFavorite: Button = itemView.findViewById(R.id.del_button)
        val context = itemView.context
        fun bind(favourite: Favourite) {
//            favouriteHelper = FavouriteHelper.getInstance(context)
//            favouriteHelper.open()
//            val uriResolver = uriWithId
            var url = favourite.photo
            with(itemView){
                Glide.with(itemView.context)
                    .load(url)
                    .apply(RequestOptions().override(150, 150))
                    .into(fav_item_photo)
                tv_item_title.text = favourite.title
                tv_item_date.text = favourite.date
                tv_item_description.text = favourite.description
                cv_item_favourite.setOnClickListener(CustomOnItemClickListener(adapterPosition, object : CustomOnItemClickListener.OnItemClickCallback {

                        override fun onItemClicked(view: View, position: Int) {
                            val alertDialogBuilder = AlertDialog.Builder(context)
                            alertDialogBuilder.setTitle("Hapus")
                            alertDialogBuilder
                                .setMessage("Apakah ingin dihapus")
                                .setCancelable(false)
                                .setPositiveButton("Ya") { dialog, id ->
                        val intent = Intent(activity, DetailFavActivity::class.java)
                        intent.putExtra(DetailFavActivity.EXTRA_POSITION, position)
                        intent.putExtra(DetailFavActivity.EXTRA_FAVOURITE, favourite)
                        activity.startActivityForResult(intent, DetailFavActivity.REQUEST_UPDATE)
                        removeItem(position)
                                }
                                .setNegativeButton("Tidak") { dialog, id -> dialog.cancel() }
                            val alertDialog = alertDialogBuilder.create()
                            alertDialog.show()

//                        val intent = Intent(activity, DetailFavActivity::class.java)
//                        intent.putExtra(DetailFavActivity.EXTRA_POSITION, position)
//                        intent.putExtra(DetailFavActivity.EXTRA_FAVOURITE, favourite)
//                        activity.startActivityForResult(intent, DetailFavActivity.REQUEST_UPDATE)
//                        removeItem(position)
                    }
                }))
            }
        }
    }
}