package com.bl.favouriteapp.db

import android.net.Uri
import android.provider.BaseColumns

object DatabaseContract {

    val AUTHORITY = "com.bl.moviecatalogue"
    val SCHEME = "content"

    class FavouriteColumns : BaseColumns {
        companion object {
            const val TABLE_NAME = "fav"
            const val _ID = "_id"
            const val TITLE = "title"
            const val DESCRIPTION = "description"
            const val DATE = "date"
            const val PHOTO = "photo"

            val CONTENT_URI: Uri = Uri.Builder().scheme(SCHEME)
                .authority(AUTHORITY)
                .appendPath(TABLE_NAME)
                .build()
        }
    }
}