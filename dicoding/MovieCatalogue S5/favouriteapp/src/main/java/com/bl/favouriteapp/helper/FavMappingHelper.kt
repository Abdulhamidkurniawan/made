package com.bl.favouriteapp.helper

import android.database.Cursor
import com.bl.favouriteapp.Tv
import com.bl.favouriteapp.db.DatabaseContract

object FavMappingHelper {

    fun mapCursorToArrayList(favouritesCursor: Cursor): ArrayList<Tv> {
        val favouritesList = ArrayList<Tv>()
        while (favouritesCursor.moveToNext()) {
            val id = favouritesCursor.getInt(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns._ID))
            val title = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.TITLE))
            val description = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.DESCRIPTION))
            val date = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.DATE))
            val photo = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.PHOTO))
            favouritesList.add(Tv(id, title, description, date, photo))
        }
        return favouritesList
    }
}