package com.bl.favouriteapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Tv(
    var id: Int = 0,
    var tv_name: String? = null,
    var tv_date: String? = null,
    var tv_description: String? = null,
    var tv_photo: String? = null

    ) : Parcelable
