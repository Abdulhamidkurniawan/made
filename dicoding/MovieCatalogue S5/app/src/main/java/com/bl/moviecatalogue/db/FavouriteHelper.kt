package com.bl.moviecatalogue.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import com.bl.moviecatalogue.Tv
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.DATE
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.DESCRIPTION
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.PHOTO
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.TABLE_NAME
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.TITLE
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion._ID
import com.loopj.android.http.AsyncHttpClient.log
import java.sql.SQLException
import java.util.ArrayList

class FavouriteHelper (context: Context) {
    private val dataBaseHelper: DatabaseHelper = DatabaseHelper(context)

    private lateinit var database: SQLiteDatabase

    companion object {
        private const val DATABASE_TABLE = TABLE_NAME
        private var INSTANCE: FavouriteHelper? = null

        fun getInstance(context: Context): FavouriteHelper {
            if (INSTANCE == null) {
                synchronized(SQLiteOpenHelper::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = FavouriteHelper(context)
                    }
                }
            }
            return INSTANCE as FavouriteHelper
        }

    }

    @Throws(SQLException::class)
    fun open() {
        database = dataBaseHelper.writableDatabase
    }

    fun close() {
        dataBaseHelper.close()

        if (database.isOpen)
            database.close()
    }

    fun queryAll(): Cursor {
        return database.query(
            DATABASE_TABLE,
            null,
            null,
            null,
            null,
            null,
            "$_ID ASC",
            null)
    }

    fun query(): ArrayList<Tv> {
        val arrayList = ArrayList<Tv>()
        val cursor = database.query(DATABASE_TABLE, null, null, null, null, null, "${BaseColumns._ID} DESC", null)
        cursor.moveToFirst()
        var tv: Tv
        if (cursor.count > 0) {
            do {
                tv = Tv()
                tv.id = cursor.getInt(cursor.getColumnIndexOrThrow(_ID))
                tv.tv_name = cursor.getString(cursor.getColumnIndexOrThrow(TITLE))
                tv.tv_date = cursor.getString(cursor.getColumnIndexOrThrow(DATE))
                tv.tv_description = cursor.getString(cursor.getColumnIndexOrThrow(DESCRIPTION))
                tv.tv_photo = cursor.getString(cursor.getColumnIndexOrThrow(PHOTO))
                arrayList.add(tv)
                cursor.moveToNext()

            } while (!cursor.isAfterLast)
        }
        cursor.close()
        return arrayList
    }

    fun queryById(id: String): Cursor {
        return database.query(DATABASE_TABLE, null, "$_ID = ?", arrayOf(id), null, null, null, null)
    }

    fun insert(values: ContentValues?): Long {
        return database.insert(DATABASE_TABLE, null, values)

    }

    fun update(id: String, values: ContentValues?): Int {
        return database.update(DATABASE_TABLE, values, "$_ID = ?", arrayOf(id))
    }

    fun deleteById(id: String): Int {
        return database.delete(DATABASE_TABLE, "$_ID = '$id'", null)
    }
}