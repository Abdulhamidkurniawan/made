package com.bl.moviecatalogue.helper

import android.database.Cursor
import com.bl.moviecatalogue.Tv
import com.bl.moviecatalogue.db.DatabaseContract
import com.bl.moviecatalogue.entity.Favourite

object FavMappingHelper {

    fun mapCursorToArrayList(favouritesCursor: Cursor): ArrayList<Tv> {
        val favouritesList = ArrayList<Tv>()
        while (favouritesCursor.moveToNext()) {
            val id = favouritesCursor.getInt(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns._ID))
            val title = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.TITLE))
            val description = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.DESCRIPTION))
            val date = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.DATE))
            val photo = favouritesCursor.getString(favouritesCursor.getColumnIndexOrThrow(DatabaseContract.FavouriteColumns.PHOTO))
            favouritesList.add(Tv(id, title, description, date, photo))
        }
        return favouritesList
    }
}