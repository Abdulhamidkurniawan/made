package com.bl.moviecatalogue.activity

import android.app.PendingIntent.getActivity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bl.moviecatalogue.MainViewModel
import com.bl.moviecatalogue.R
import com.bl.moviecatalogue.Tv
import com.bl.moviecatalogue.adapter.FavouriteAdapter
import com.bl.moviecatalogue.adapter.ListTvAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.fragment_tv.*

class SearchActivity : AppCompatActivity() {
    private lateinit var adapter: ListTvAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var mainViewModel: MainViewModel
    private lateinit var query: String
    val EXTRA_QUERY = "extra_query"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        query = intent.getStringExtra(EXTRA_QUERY)
        supportActionBar?.title = "Search : "+query

        progressBar = findViewById(R.id.progressBar)

        adapter = ListTvAdapter()
        adapter.notifyDataSetChanged()

        rv_search.layoutManager = LinearLayoutManager(this)
        rv_search.setHasFixedSize(true)
//        adapter = ListTvAdapter()
        rv_search.adapter = adapter

        mainViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MainViewModel::class.java)
        val arrayAdapter = ArrayAdapter<String>(
            applicationContext, android.R.layout.simple_spinner_dropdown_item,
            resources.getStringArray(R.array.search_filter_array)
        )

        mainViewModel.getTv().observe(this, Observer { tvItems ->
            if (tvItems != null) {
                adapter.setData(tvItems)
            }
            showLoading(false)
        })

        spinner_search_result.adapter = arrayAdapter
        spinner_search_result.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                mainViewModel.searchMoviesTVShows("movie", query)
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                if (spinner_search_result.selectedItemPosition == 0) {
                    mainViewModel.searchMoviesTVShows("movie", query)
                }
                else if (spinner_search_result.selectedItemPosition == 1) {
                    mainViewModel.searchMoviesTVShows("tv", query)
                }
            }

        }


        adapter.setOnItemClickCallback(object :
            ListTvAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Tv) {
                showSelectedTv(data)
                val moveWithObjectIntent = Intent(this@SearchActivity, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MOVIE, data )
                startActivity(moveWithObjectIntent)

            }
        })
//        mainViewModel.getListMovies()
        showLoading(true)
    }

    private fun showSelectedTv(tv: Tv) {
        Toast.makeText(this@SearchActivity, "Kamu memilih ${tv.tv_name}", Toast.LENGTH_SHORT).show()

    }

    private fun showLoading(state: Boolean) {
        if (state) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        if (searchManager != null){
            val searchView = (menu?.findItem(R.id.action_search_main)?.actionView) as SearchView
//            type = navView.selectedItemId.toString()
            searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
            searchView.queryHint = resources.getString(R.string.search_hint)
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    Toast.makeText(applicationContext, query, Toast.LENGTH_SHORT).show()
                    val intent = Intent(applicationContext, SearchActivity::class.java)
                    intent.putExtra(SearchActivity().EXTRA_QUERY, query)
                    startActivity(intent)
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }

            })

        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_change_settings) {
            val mIntent = Intent(Settings.ACTION_LOCALE_SETTINGS)
            startActivity(mIntent)
        }
        return super.onOptionsItemSelected(item)
    }
}

