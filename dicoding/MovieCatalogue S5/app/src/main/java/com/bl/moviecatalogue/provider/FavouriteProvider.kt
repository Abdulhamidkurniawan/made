package com.bl.moviecatalogue.provider

import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.content.UriMatcher
import android.database.Cursor
import android.net.Uri
import android.os.Build.ID
import android.os.Handler
import com.bl.moviecatalogue.activity.DetailFavActivity
import com.bl.moviecatalogue.adapter.FavouriteAdapter
import com.bl.moviecatalogue.db.DatabaseContract
import com.bl.moviecatalogue.db.DatabaseContract.AUTHORITY
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion._ID
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.CONTENT_URI
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.TABLE_NAME
import com.bl.moviecatalogue.db.FavouriteHelper
import com.bl.moviecatalogue.fragment.FavouriteFragment
import com.bl.moviecatalogue.fragment.ProviderFavouriteFragment


class FavouriteProvider : ContentProvider() {

    companion object {
        lateinit var adapter: FavouriteAdapter

        private const val FAV = 1
        private const val FAV_ID = 2
        private lateinit var favouriteHelper: FavouriteHelper

        private val sUriMatcher = UriMatcher(UriMatcher.NO_MATCH)

        init {
            // content://com.dicoding.picodiploma.mynotesapp/note
            sUriMatcher.addURI(AUTHORITY, TABLE_NAME, FAV)

            // content://com.dicoding.picodiploma.mynotesapp/note/id
            sUriMatcher.addURI(AUTHORITY, "$TABLE_NAME/#", FAV_ID)
        }
    }
    override fun onCreate(): Boolean {
        favouriteHelper = FavouriteHelper.getInstance(context as Context)
        favouriteHelper.open()
        return true
    }

    override fun query(uri: Uri, strings: Array<String>?, s: String?, strings1: Array<String>?, s1: String?): Cursor? {
        val cursor: Cursor?
        when (sUriMatcher.match(uri)) {
            FAV -> cursor = favouriteHelper.queryAll()
            FAV_ID -> cursor = favouriteHelper.queryById(uri.lastPathSegment.toString())
            else -> cursor = null
        }
        return cursor
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, contentValues: ContentValues?): Uri? {
        val added: Long = when (FAV) {
            sUriMatcher.match(uri) -> favouriteHelper.insert(contentValues)
            else -> 0
        }
        context?.contentResolver?.notifyChange(CONTENT_URI, null)

        return Uri.parse("$CONTENT_URI/$added")
    }



    override fun update(uri: Uri, contentValues: ContentValues?, s: String?, strings: Array<String>?): Int {
        val updated: Int = when (FAV_ID) {
            sUriMatcher.match(uri) -> favouriteHelper.update(uri.lastPathSegment.toString(),contentValues)
            else -> 0
        }
        context?.contentResolver?.notifyChange(CONTENT_URI, null)

        return updated
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        val deleted: Int = when (FAV_ID) {
            sUriMatcher.match(uri) -> favouriteHelper.deleteById(uri.lastPathSegment.toString())
            else -> 0
        }
        context?.contentResolver?.notifyChange(CONTENT_URI, null)

        return deleted
    }

}
