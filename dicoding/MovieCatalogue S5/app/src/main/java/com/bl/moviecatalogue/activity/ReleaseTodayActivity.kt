package com.bl.moviecatalogue.activity


import android.app.PendingIntent.getActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bl.moviecatalogue.MainViewModel
import com.bl.moviecatalogue.R
import com.bl.moviecatalogue.Tv
import com.bl.moviecatalogue.adapter.ListTvAdapter
import kotlinx.android.synthetic.main.activity_release_today.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ReleaseTodayActivity : AppCompatActivity() {
    private lateinit var adapter: ListTvAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var mainViewModel: MainViewModel
    val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    val todayDate = dateFormat.format(Date())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_release_today)

        supportActionBar?.title = getString(R.string.today_release)+" : "+todayDate

        progressBar_release_today.visibility = View.VISIBLE
        adapter = ListTvAdapter()
        adapter.notifyDataSetChanged()

        rv_release.setHasFixedSize(true)
        rv_release.layoutManager = LinearLayoutManager(this)
        rv_release.adapter = adapter

        mainViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            MainViewModel::class.java)
        mainViewModel.getTv().observe(this, Observer { tvItems ->
            if (tvItems != null) {
                adapter.setData(tvItems)
            }
            showLoading(false)
        })

        adapter.setOnItemClickCallback(object :
            ListTvAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Tv) {
                showSelectedTv(data)
                val moveWithObjectIntent = Intent(this@ReleaseTodayActivity, DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MOVIE, data )
                startActivity(moveWithObjectIntent)

            }
        })
        mainViewModel.getReleaseMovies()
        showLoading(true)

//        if (intent.getSerializableExtra("movieList") != null) {
//            showMovies()
//        }
    }

    private fun showLoading(state: Boolean) {
        if (state) {
            progressBar_release_today.visibility = View.VISIBLE
        } else {
            progressBar_release_today.visibility = View.GONE
        }
    }

    private fun showSelectedTv(tv: Tv) {
        Toast.makeText(this, "Kamu memilih ${tv.tv_name}", Toast.LENGTH_SHORT).show()
    }

//    fun showMovies() {
//        val movies = intent.getSerializableExtra("movieList") as ArrayList<Tv>
//        rv_release.apply {  }
//        adapter.setOnItemClickCallback(object :
//            ListTvAdapter.OnItemClickCallback {
//            override fun onItemClicked(data: Tv) {
//                showSelectedTv(data)
//                val moveWithObjectIntent = Intent(this@ReleaseTodayActivity, DetailActivity::class.java)
//                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MOVIE, data )
//                startActivity(moveWithObjectIntent)
//
//            }
//        })
//
//        rv_release.layoutManager = LinearLayoutManager(this)
//        progressBar_release_today.visibility = View.GONE
//    }
}
