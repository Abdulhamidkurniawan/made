package com.bl.moviecatalogue

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel : ViewModel() {
    companion object {
        private const val API_KEY = BuildConfig.TMDB_API_KEY
    }
    val listMovies = MutableLiveData<ArrayList<Tv>>()
    val listTvs = MutableLiveData<ArrayList<Tv>>()

    internal fun getTv(): LiveData<ArrayList<Tv>> {
        return listTvs
    }
    internal fun getMovies(): LiveData<ArrayList<Tv>> {
        return listMovies
    }

    internal fun getListMovies() {
        val client = AsyncHttpClient()
        val listItems = ArrayList<Tv>()
        val url = "https://api.themoviedb.org/3/discover/movie?api_key=$API_KEY&language=en-US"
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray) {
                try {
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)
                    val list = responseObject.getJSONArray("results")
                    for (i in 0 until list.length()) {
                        val json = list.getJSONObject(i)
                        val tvItems = Tv()
                        tvItems.id = json.getInt("id")
                        tvItems.tv_name = json.getString("title")
                        tvItems.tv_date = json.getString("release_date")
                        tvItems.tv_description = json.getString("overview")
                        tvItems.tv_photo = json.getString("poster_path")
                        listItems.add(tvItems)
                    }
                    listTvs.postValue(listItems)
                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }
            override fun onFailure(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray, error: Throwable) {
                Log.d("onFailure", error.message.toString())
            }
        })
    }

    internal fun getListTvs() {
        val client = AsyncHttpClient()
        val listItems = ArrayList<Tv>()
        val url = "https://api.themoviedb.org/3/discover/tv?api_key=$API_KEY&language=en-US"
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray) {
                try {
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)
                    val list = responseObject.getJSONArray("results")
                    for (i in 0 until list.length()) {
                        val json = list.getJSONObject(i)
                        val tvItems = Tv()
                        tvItems.id = json.getInt("id")
                        tvItems.tv_name = json.getString("original_name")
                        tvItems.tv_date = json.getString("first_air_date")
                        tvItems.tv_description = json.getString("overview")
                        tvItems.tv_photo = json.getString("poster_path")
                        listItems.add(tvItems)
                    }
                    listTvs.postValue(listItems)
                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }
            override fun onFailure(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray, error: Throwable) {
                Log.d("onFailure", error.message.toString())
            }
        })
    }

    internal fun searchMoviesTVShows(type: String, query: String) {
        val client = AsyncHttpClient()
        val listItems = ArrayList<Tv>()
        val query = query
        val jenis = type
        if (jenis == "movie"){
            val url = "https://api.themoviedb.org/3/search/movie?api_key=$API_KEY&language=en-US&query=$query"
            client.get(url, object : AsyncHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray) {
                    try {
                        val result = String(responseBody)
                        val responseObject = JSONObject(result)
                        val list = responseObject.getJSONArray("results")
                        for (i in 0 until list.length()) {
                            val json = list.getJSONObject(i)
                            val tvItems = Tv()
                            tvItems.id = json.getInt("id")
                            tvItems.tv_name = json.getString("title")
                            tvItems.tv_date = json.getString("release_date")
                            tvItems.tv_description = json.getString("overview")
                            tvItems.tv_photo = json.getString("poster_path")
                            listItems.add(tvItems)
                        }
                        listTvs.postValue(listItems)
                    } catch (e: Exception) {
                        Log.d("Exception", e.message.toString())
                    }
                }
                override fun onFailure(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray, error: Throwable) {
                    Log.d("onFailure", error.message.toString())
                }
            })
        }
        else if (jenis == "tv"){
            val url = "https://api.themoviedb.org/3/search/tv?api_key=$API_KEY&language=en-US&query=$query"
            client.get(url, object : AsyncHttpResponseHandler() {
                override fun onSuccess(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray) {
                    try {
                        val result = String(responseBody)
                        val responseObject = JSONObject(result)
                        val list = responseObject.getJSONArray("results")
                        for (i in 0 until list.length()) {
                            val json = list.getJSONObject(i)
                            val tvItems = Tv()
                            tvItems.id = json.getInt("id")
                            tvItems.tv_name = json.getString("original_name")
                            tvItems.tv_date = json.getString("first_air_date")
                            tvItems.tv_description = json.getString("overview")
                            tvItems.tv_photo = json.getString("poster_path")
                            listItems.add(tvItems)
                        }
                        listTvs.postValue(listItems)
                    } catch (e: Exception) {
                        Log.d("Exception", e.message.toString())
                    }
                }
                override fun onFailure(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray, error: Throwable) {
                    Log.d("onFailure", error.message.toString())
                }
            })
        }

    }
    internal fun getReleaseMovies() {
        val client = AsyncHttpClient()
        val listItems = ArrayList<Tv>()
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        val todayDate = dateFormat.format(Date())
        val url = "https://api.themoviedb.org/3/discover/movie?api_key=$API_KEY&primary_release_date.gte=$todayDate&primary_release_date.lte=$todayDate"
        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray) {
                try {
                    val result = String(responseBody)
                    val responseObject = JSONObject(result)
                    val list = responseObject.getJSONArray("results")
                    for (i in 0 until list.length()) {
                        val json = list.getJSONObject(i)
                        val tvItems = Tv()
                        tvItems.id = json.getInt("id")
                        tvItems.tv_name = json.getString("title")
                        tvItems.tv_date = json.getString("release_date")
                        tvItems.tv_description = json.getString("overview")
                        tvItems.tv_photo = json.getString("poster_path")
                        listItems.add(tvItems)
                    }
                    listTvs.postValue(listItems)
                } catch (e: Exception) {
                    Log.d("Exception", e.message.toString())
                }
            }
            override fun onFailure(statusCode: Int, headers: Array<Header>?, responseBody: ByteArray, error: Throwable) {
                Log.d("onFailure", error.message.toString())
            }
        })
    }
}