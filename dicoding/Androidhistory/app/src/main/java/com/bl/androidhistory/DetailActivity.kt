package com.bl.androidhistory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class DetailActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val detailReceived: TextView = findViewById(R.id.detail_received)
        val nameReceived: TextView = findViewById(R.id.name_received)
        val photoReceived: ImageView = findViewById(R.id.photo_received)

        val name = intent.getStringExtra(EXTRA_NAME)
        val detail = intent.getStringExtra(EXTRA_DETAIL)
        val photo = intent.getIntExtra(EXTRA_PHOTO,0)
        val btnMoveActivity: Button = findViewById(R.id.btn_back)
        setActionBarTitle(name)
        btnMoveActivity.setOnClickListener(this)


//          val text = "Name : $name"
//          tvDataReceived.text = text
            detailReceived.text = detail
            nameReceived.text = name

        Glide.with(this)
            .load(photo)
            .apply(RequestOptions().override(500, 500))
            .into(photoReceived)
    }

    companion object {
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_DETAIL = "extra_detail"
        const val EXTRA_PHOTO = "extra_photo"
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> {
                val moveIntent = Intent(this@DetailActivity, MainActivity::class.java)
                startActivity(moveIntent)
            }
        }
    }

    private fun setActionBarTitle(name: String) {
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).title = name
        }
    }

}
