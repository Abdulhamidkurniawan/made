package com.bl.androidhistory

data class Android(
    var name: String = "",
    var detail: String = "",
    var photo: Int = 0
)