package com.bl.androidhistory

object AndroidsData {
    private val androidNames = arrayOf("Android 1.0",
        "Android 1.5 Cupcake ",
        "Android 1.6 Donut",
        "Android 2.0 Eclair",
        "Android 2.2 Froyo",
        "Android 2.3 Gingerbread",
        "Android 3.0 Honeycomb",
        "Android 4.0 Ice Cream Sandwich",
        "Android 4.1 Jellybean",
        "Android 4.4 Kitkat",
        "Android 5.0 Lollipop",
        "Android 6.0 Marshmallow",
        "Android 7.0 Nougat",
        "Android 8.0 Oreo",
        "Android 9.0 Pie",
        "Android 10.0")

    private val androidDetails = arrayOf("Platform Android pertama kali diluncurkan pada September 2008, Sebelum dinamai dengan nama Apple pie/Alpha versi android ini dinamai dengan nama Astro akan tetapi karena alasan hak cipta, versi ini diganti dengan nama Apple Pie/Alpha",
        "Baru pada April 2009 Google memakai nama cemilan untuk pertama kali pada versi android. Versi ini berbasi kernel Linux 2.6.27. Dalam versi ini pengguna dapat meng-upload video ke Youtube serta meng-upload gambar ke Picasa, mulai disinilah terdapat Widget yang dapat dibesar/kecilkan.",
        "Donut 1.6 ini sendiri dirilis pada 15 September 2009. Android versi Donut memiliki beberapa fitur yang lebih baik dibanding dengan pendahulunya, pada fitur pencarian dan UI yang lebih user friendly.",
        "Pada tanggal 9 desember 2009 lahirlah OS Eclair 2.0-2.1. Dari sinilah Android mulai tidak dilihat sebelah mata, karena pada awalnya penggunaan sistem layar yang di anggap kurang friendly, banyak perusahaan perusahaan pengembang gadget yang tertarik untuk menggunakan sistem operasi Android.",
        "Lima Bulan setelah perilisan Eclair, Android versi terbaru kembali lahir pada 20 Mei 2010. Karena Android mulai maju membuat banyak orang yang mendiskusikan tentang persaingan ketat antara Android dan iOS. Keinginan untuk penggunaan memori eksternal di O.S Android dapat di wujudkan dari sini, jenis memori yang digunakan yaitu SDcard.",
        "Masih dengan urutan Abjad, Gingerbread diperkenalkan pada tanggal 6 Desember 2010.  Disini para pengguna mulai dapat menggunakan fitur dual yang memungkinkan untuk penggunan Video Call, dan juga disini Android mulai meningkatkan mutu untuk aplikasi aplikasi dan permainan.",
        "Honeycomb versi 3.0 dirilis pada 22 Februari 2011. Versi ini di khususkan untuk digunakan di perangkat tablet, tentu saja tampilannya berbeda dengan versi android untuk smartphone.",
        "Android Ice Cream Sandwich 4.0 dirilis pada 19 oktober 2011. Penambahan mode buka kunci dengan identifikasi wajah pertama kali diususng pada versi ini, fitur yang bisa dibilang sangat keren untuk sebuah smartphone. Kemudian dari sisi browser bawaan Chrome, mampu memuat halaman hingga 16 tab.",
        "Android versi Jelly Bean yang pertama diluncurkan tepat 9 Juli 2012. Versi ini ialah upaya pertama Google untuk menghadirkan asisten digital yang dinamai dengan Google Now. Mulai dari versi ini, Google semakin berhasrat untuk membuat asisten digital yang lebih hidup, manusiawi, dan relevan bagi penggunanya.",
        "Android KitKat dirilis pada 31 Oktober 2013 dan menjadi salah satu versi Android yang paling disukai oleh pengguna Smartphone di dunia. Mengapa demikian? KitKat memiliki fitur yang istimewa dari OS Android sebelumnya. Adanya optimasi kinerja untuk smartphone dengan spesifikasi rendah.",
        "Android 5.0  atau yang lebih dikenal dengan nama Lollipop pertama kali diperkenalkan pada 25 Juni 2014. Bertepatan dengan momen tersebut, beragam hasil produk teknologi dari Google juga diperkenalkan. Antara lain Android TV dan platform pelacakan kesehatan Google Fit.",
        "Sistem proteksi Android dengan metodesidik jari pertama sekali dirancang pada OS Android Marshmallow. Pengunaan proteksi sidik jari pada Android M ini bisa digunakan untuk proses otentikasi Play Store dan pembelian dengan sistem Android Pay.",
        "Android versi Nougat dirilis pada 22 Agustus 2016. Di versi ini assistant virtual Google Now di gantikan dengan asisstant virtual yang bernama Google Assistant.",
        "Pada 21 agustus 2017 android oreo diluncurkan ke publik untuk ke semua smartphone yang menggunakan sistem operasi android. Versi ini membawa beberapa fitur baru seperti background limit untuk menghemat baterai dan data, notification channel dan dots untuk mempermudah akses ke hal-hal penting, picture in picture untuk menunjang produktivitas dengan dua aplikasi pada satu waktu, serta keamanan yang diklaim lebih mumpuni.",
        "Android Pie,dengan kode nama sebagai Android P, pertama kali diumumkan oleh Google pada 7 Maret 2018 dan versi pengembangan pertama dirilis pada hari yang sama. Google membawa 2 slogan utama yang sangat bermanfaat dan unik pada android versi 9, yaitu “adaptive battery dan adaptive brigthness”. Para peracik sistem operasi ini mengetahui betul kebutuhan pengguna dalam menggunakan smartphone, terutama pada masalah baterai.",
        "Android 10 telah resmi meluncur awal September lalu. Tak lama kemudian, Google memperkenalkan Android 10 Go yang tak lain merupakan Android 10 versi ringan. Untuk versi terbaru ini, Google akan fokus pada kecepatan untuk pengoperasian perangkat. Google mengklaim Android Go terbarunya 10 persen lebih gegas dibanding Android 9 Go.")

    private val androidsImages = intArrayOf(R.drawable.android,
        R.drawable.cupcake,
        R.drawable.donut,
        R.drawable.eclairs,
        R.drawable.froyo,
        R.drawable.gingerbread,
        R.drawable.honeycomb,
        R.drawable.ics,
        R.drawable.jellybean,
        R.drawable.kitkat,
        R.drawable.lollipop,
        R.drawable.marshmallow,
        R.drawable.nougat,
        R.drawable.oreo,
        R.drawable.pie,
        R.drawable.aq)

    val listData: ArrayList<Android>
        get() {
            val list = arrayListOf<Android>()
            for (position in androidNames.indices) {
                val android = Android()
                android.name = androidNames[position]
                android.detail = androidDetails[position]
                android.photo = androidsImages[position]
                list.add(android)
            }
            return list
        }
}