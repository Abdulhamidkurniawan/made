package com.bl.androidhistory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    private lateinit var rvAndroids: RecyclerView
    private var list: ArrayList<Android> = arrayListOf()
    private var title: String = "Android OS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvAndroids = findViewById(R.id.rv_androids)
        rvAndroids.setHasFixedSize(true)

        list.addAll(AndroidsData.listData)
        showRecyclerList()
        setActionBarTitle(title)

    }

    private fun showRecyclerList() {
        rvAndroids.layoutManager = LinearLayoutManager(this)
        val listAndroidAdapter = ListAndroidAdapter(list)
        rvAndroids.adapter = listAndroidAdapter

        listAndroidAdapter.setOnItemClickCallback(object : ListAndroidAdapter.OnItemClickCallback {

            override fun onItemClicked(data: Android) {
                val move = Intent(this@MainActivity, DetailActivity::class.java)
                move.putExtra(DetailActivity.EXTRA_NAME, data.name)
                move.putExtra(DetailActivity.EXTRA_DETAIL, data.detail)
                move.putExtra(DetailActivity.EXTRA_PHOTO, data.photo)

                startActivity(move)

                showSelectedAndroid(data)
            }
        })
    }

    private fun showSelectedAndroid(android: Android) {
        Toast.makeText(this, "Kamu memilih " + android.name, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        setMode(item.itemId)
        return super.onOptionsItemSelected(item)
    }

    private fun setMode(selectedMode: Int) {
        when (selectedMode) {
            R.id.action_profil -> {
                val moveIntent = Intent(this@MainActivity, Profil::class.java)
                title = "Profil"
                startActivity(moveIntent)
            }

        }
        setActionBarTitle(title)
    }

    private fun setActionBarTitle(title: String) {
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).title = title
        }
    }
}
