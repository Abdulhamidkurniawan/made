package com.bl.androidhistory

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.ActionBar

class Profil : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil)
        var title: String = "Profil"
        val btnMoveActivity: Button = findViewById(R.id.btn_back)
        btnMoveActivity.setOnClickListener(this)
        setActionBarTitle(title)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> {
                val moveIntent = Intent(this@Profil, MainActivity::class.java)
                startActivity(moveIntent)
            }
        }
    }

    private fun setActionBarTitle(title: String) {
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).title = title
        }
    }
}
