package com.bl.moviecatalogue


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_tv.*

/**
 * A simple [Fragment] subclass.
 */
class MovieFragment : Fragment() {
    private val list = ArrayList<Tv>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inf = inflater.inflate(R.layout.fragment_tv, container, false)

        return inf
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_tvs.setHasFixedSize(true)

        list.addAll(getListTvs())
        showRecyclerList()
    }

    fun getListTvs(): ArrayList<Tv> {
        val dataName = resources.getStringArray(R.array.mv_name)
        val dataDate = resources.getStringArray(R.array.mv_date)
        val dataDescription = resources.getStringArray(R.array.mv_description)
        val dataPhoto = resources.obtainTypedArray(R.array.mv_photo)
        val listTv = ArrayList<Tv>()
        for (position in dataName.indices) {
            val tv = Tv(
                dataName[position],
                dataDate[position],
                dataDescription[position],
                dataPhoto.getResourceId(position, -1)
            )
            listTv.add(tv)
        }
        return listTv
    }

    private fun showRecyclerList() {
        rv_tvs.layoutManager = LinearLayoutManager(context)
        val ListTvAdapter = ListTvAdapter(list)
        rv_tvs.adapter = ListTvAdapter

        ListTvAdapter.setOnItemClickCallback(object : ListTvAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Tv) {
                showSelectedTv(data)
                val moveWithObjectIntent = Intent(getActivity(), DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MOVIE, data )
                startActivity(moveWithObjectIntent)

            }
        })
    }

    private fun showSelectedTv(tv: Tv) {
        Toast.makeText(getActivity(), "Kamu memilih ${tv.tv_name}", Toast.LENGTH_SHORT).show()

    }
}
