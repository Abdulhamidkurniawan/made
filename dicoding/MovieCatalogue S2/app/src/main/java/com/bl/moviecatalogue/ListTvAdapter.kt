package com.bl.moviecatalogue

import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_tv.view.*

class ListTvAdapter(private val listTv: ArrayList<Tv>) : RecyclerView.Adapter<ListTvAdapter.ListViewHolder>() {
    private var onItemClickCallback: OnItemClickCallback? = null
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_tv, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.bind(listTv[position])
    }

    override fun getItemCount(): Int = listTv.size

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tv: Tv) {
            with(itemView) {
                Glide.with(itemView.context)
                    .load(tv.tv_photo)
                    .apply(RequestOptions().override(55, 55))
                    .into(tv_item_photo)
                tv_item_name.text = tv.tv_name
                tv_item_date.text = tv.tv_date
                tv_item_description.text = tv.tv_description

                itemView.setOnClickListener { onItemClickCallback?.onItemClicked(tv) }
            }
        }
    }
    interface OnItemClickCallback {
        fun onItemClicked(data: Tv)
    }
}