package com.bl.moviecatalogue

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Tv(
    var tv_name: String,
    var tv_date: String,
    var tv_description: String,
    var tv_photo: Int

    ) : Parcelable
