package com.bl.moviecatalogue


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_tv.*

/**
 * A simple [Fragment] subclass.
 */
class TvFragment : Fragment() {
    private lateinit var adapter: ListTvAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var mainViewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val inf = inflater.inflate(R.layout.fragment_tv, container, false)

        return inf
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.progressBar)

        adapter = ListTvAdapter()
        adapter.notifyDataSetChanged()

        rv_tvs.setHasFixedSize(true)
        rv_tvs.layoutManager = LinearLayoutManager(context)
        rv_tvs.adapter = adapter

        mainViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(MainViewModel::class.java)
        mainViewModel.getTv().observe(this, Observer { tvItems ->
            if (tvItems != null) {
                adapter.setData(tvItems)
            }
            showLoading(false)
        })

        adapter.setOnItemClickCallback(object : ListTvAdapter.OnItemClickCallback{
            override fun onItemClicked(data: Tv) {
                showSelectedTv(data)
                val moveWithObjectIntent = Intent(getActivity(), DetailActivity::class.java)
                moveWithObjectIntent.putExtra(DetailActivity.EXTRA_MOVIE, data )
                startActivity(moveWithObjectIntent)

            }
        })
        mainViewModel.getListTvs()
        showLoading(true)
    }

    private fun showSelectedTv(tv: Tv) {
        Toast.makeText(getActivity(), "Kamu memilih ${tv.tv_name}", Toast.LENGTH_SHORT).show()

    }

    private fun showLoading(state: Boolean) {
        if (state) {
            progressBar.visibility = View.VISIBLE
        } else {
            progressBar.visibility = View.GONE
        }
    }

}


