package com.bl.moviecatalogue

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide

class DetailActivity : AppCompatActivity(), View.OnClickListener {
    companion object {
        const val EXTRA_MOVIE = "extra_movie"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val descriptionReceived: TextView = findViewById(R.id.detail_received)
        val nameReceived: TextView = findViewById(R.id.name_received)
        val dateReceived: TextView = findViewById(R.id.date_received)
        val photoReceived: ImageView = findViewById(R.id.photo_received)

        val data = intent.getParcelableExtra(EXTRA_MOVIE) as Movie
        val btnMoveActivity: Button = findViewById(R.id.btn_back)
        val name = data.name

        setActionBarTitle(name)
        btnMoveActivity.setOnClickListener(this)

        descriptionReceived.text = data.description
        nameReceived.text = data.name
        dateReceived.text = data.date
        Glide.with(this).load(data.photo).into(photoReceived)

    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_back -> {
                val moveIntent = Intent(this@DetailActivity, MainActivity::class.java)
                startActivity(moveIntent)
            }
        }
    }

    private fun setActionBarTitle(name: String?) {
        if (supportActionBar != null) {
            (supportActionBar as ActionBar).title = name
        }
    }

}

