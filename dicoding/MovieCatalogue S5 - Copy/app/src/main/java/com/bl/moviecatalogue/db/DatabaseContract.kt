package com.bl.moviecatalogue.db

import android.provider.BaseColumns

internal class DatabaseContract {

    internal class FavouriteColumns : BaseColumns {
        companion object {
            const val TABLE_NAME = "fav"
            const val _ID = "_id"
            const val TITLE = "title"
            const val DESCRIPTION = "description"
            const val DATE = "date"
            const val PHOTO = "photo"
        }
    }
}