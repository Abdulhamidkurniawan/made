package com.bl.moviecatalogue.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns
import com.bl.moviecatalogue.db.DatabaseContract.FavouriteColumns.Companion.TABLE_NAME

internal class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {

        private const val DATABASE_NAME = "dbfavapp"

        private const val DATABASE_VERSION = 1

        private const val SQL_CREATE_TABLE_NOTE = "CREATE TABLE $TABLE_NAME" +
                "(${FavouriteColumns._ID} INTEGER PRIMARY KEY," +
                " ${FavouriteColumns.TITLE} TEXT NOT NULL," +
                " ${FavouriteColumns.DESCRIPTION} TEXT NOT NULL," +
                " ${FavouriteColumns.DATE} TEXT NOT NULL," +
                " ${FavouriteColumns.PHOTO} TEXT NOT NULL)"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_TABLE_NOTE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }
}