package com.bl.moviecatalogue.adapter

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.bl.moviecatalogue.CustomOnItemClickListener
import com.bl.moviecatalogue.FavouriteAddUpdateActivity
import com.bl.moviecatalogue.FavouriteAddUpdateActivity.Companion.EXTRA_POSITION
import com.bl.moviecatalogue.R
import com.bl.moviecatalogue.activity.DetailActivity
import com.bl.moviecatalogue.db.FavouriteHelper
import com.bl.moviecatalogue.entity.Favourite
import com.bl.moviecatalogue.fragment.FavouriteFragment
import com.bl.moviecatalogue.helper.MappingHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_favourite.view.*


class FavouriteAdapter (private val activity: Activity) : RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder>() {
    private lateinit var favouriteHelper: FavouriteHelper

    var listFavourites = ArrayList<Favourite>()
        set(listFavourites) {
            if (listFavourites.size > 0) {
                this.listFavourites.clear()
            }
            this.listFavourites.addAll(listFavourites)

            notifyDataSetChanged()
        }

    fun addItem(favourite: Favourite) {
        this.listFavourites.add(favourite)
        notifyItemInserted(this.listFavourites.size - 1)
    }

    fun updateItem(position: Int, favourite: Favourite) {
        this.listFavourites[position] = favourite
        notifyItemChanged(position, favourite)
    }

    fun removeItem(position: Int) {
        this.listFavourites.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, this.listFavourites.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_favourite, parent, false)
        return FavouriteViewHolder(view)
    }

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        val context = holder.itemView.context
        val dialogTitle: String
        val dialogMessage: String

        dialogMessage = "Apakah anda yakin ingin menghapus item ini?"
        dialogTitle = "Hapus Favourite"
        favouriteHelper = FavouriteHelper.getInstance(context)
        holder.bind(listFavourites[position])
        holder.delFavorite.setOnClickListener {
        val alertDialogBuilder = AlertDialog.Builder(context)
        alertDialogBuilder.setTitle(dialogTitle)
        alertDialogBuilder
            .setMessage(dialogMessage)
            .setCancelable(false)
            .setPositiveButton("Ya") { dialog, id ->
                val result = favouriteHelper.deleteById(listFavourites[position].id.toString()).toLong()
                if (result > 0) {
                    Toast.makeText(holder.itemView.context, "Hapus " + listFavourites[position].title, Toast.LENGTH_SHORT).show()
                    removeItem(position)
                } else {
                    Toast.makeText(holder.itemView.context, "Gagal menghapus data", Toast.LENGTH_SHORT).show()
                }
                }

            .setNegativeButton("Tidak") { dialog, id -> dialog.cancel() }
            val alertDialog = alertDialogBuilder.create()
            alertDialog.show()
            favouriteHelper.open()

        }
    }

    override fun getItemCount(): Int = this.listFavourites.size

    inner class FavouriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var delFavorite: Button = itemView.findViewById(R.id.del_button)
        val context = itemView.context

        fun bind(favourite: Favourite) {
            favouriteHelper = FavouriteHelper.getInstance(context)
            favouriteHelper.open()
            var url = favourite.photo
            with(itemView){
                Glide.with(itemView.context)
                    .load(url)
                    .apply(RequestOptions().override(150, 150))
                    .into(fav_item_photo)
                tv_item_title.text = favourite.title
                tv_item_date.text = favourite.date
                tv_item_description.text = favourite.description
//                cv_item_favourite.setOnClickListener(CustomOnItemClickListener(adapterPosition, object : CustomOnItemClickListener.OnItemClickCallback {
//                    override fun onItemClicked(view: View, position: Int) {
//                        val intent = Intent(activity, FavouriteAddUpdateActivity::class.java)
//                        intent.putExtra(FavouriteAddUpdateActivity.EXTRA_POSITION, position)
//                        intent.putExtra(FavouriteAddUpdateActivity.EXTRA_FAVOURITE, favourite)
//                        activity.startActivityForResult(intent, FavouriteAddUpdateActivity.REQUEST_UPDATE)
//
//                    }
//                }))
            }
        }
    }
}