package com.bl.myreadwritefile

data class FileModel(
    var filename: String? = null,
    var data: String? = null
)