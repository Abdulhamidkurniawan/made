package com.bl.moviecatalogue.widget

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Binder
import android.os.Bundle
import android.util.Log
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.core.os.bundleOf
import com.bl.moviecatalogue.R
import com.bl.moviecatalogue.Tv
import com.bl.moviecatalogue.db.FavouriteHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target

class StackRemoteViewsFactory(val context: Context) : RemoteViewsService.RemoteViewsFactory {

    private val widgetItem = ArrayList<String>()
    private val itemName=ArrayList<String>()
    private val dataFilm=ArrayList<Tv>()
    private var mContext: Context=context

    override fun onCreate() {
        getLoadData()
    }

    override fun onDataSetChanged() {
        getLoadData()

    }

    override fun onDestroy() {

    }

    override fun getCount(): Int = dataFilm.size

    override fun getViewAt(position: Int): RemoteViews {
        val rv=RemoteViews(mContext.packageName, R.layout.widget_item)
        if(widgetItem.size!=0) {
            val bitmap = Glide.with(mContext)
                .asBitmap()
                .load(widgetItem[position])
                .submit().get()
            rv.setImageViewBitmap(R.id.imageView, bitmap)

//            val extras = Bundle()
//            extras.putString(ImagesBannerWidget().EXTRA_ITEM, itemName[position])
            val extras = bundleOf(ImagesBannerWidget().EXTRA_ITEM to position)

            val fillInIntent = Intent()
            fillInIntent.putExtras(extras)

            rv.setOnClickFillInIntent(R.id.imageView,fillInIntent)
        }
        return rv
    }

    private fun getLoadData(){
        widgetItem.clear()
        itemName.clear()
        dataFilm.clear()
        // Movie
        val favouriteHelper= FavouriteHelper(mContext)
        favouriteHelper.open()
        if (favouriteHelper.query().isNotEmpty()){
            dataFilm.addAll(favouriteHelper.query())
        }
        favouriteHelper.close()

        for(tv in dataFilm){
            widgetItem.add(tv.tv_photo.toString())
            itemName.add(tv.tv_name.toString())
        }
    }

    override fun getLoadingView(): RemoteViews? = null

    override fun getViewTypeCount(): Int = 1

    override fun getItemId(i: Int): Long = 0

    override fun hasStableIds(): Boolean = false

}