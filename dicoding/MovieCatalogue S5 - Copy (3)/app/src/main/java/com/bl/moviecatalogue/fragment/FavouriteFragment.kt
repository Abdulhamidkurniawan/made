package com.bl.moviecatalogue.fragment

import android.content.Intent
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bl.moviecatalogue.adapter.FavouriteAdapter
import com.bl.moviecatalogue.db.FavouriteHelper
import com.bl.moviecatalogue.entity.Favourite
import com.bl.moviecatalogue.helper.MappingHelper
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_favourite.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.bl.moviecatalogue.FavouriteAddUpdateActivity
import com.bl.moviecatalogue.R


/**
 * A simple [Fragment] subclass.
 */
class FavouriteFragment : Fragment() {
    private lateinit var adapter: FavouriteAdapter
    private lateinit var favouriteHelper: FavouriteHelper

    companion object {
        private const val EXTRA_STATE = "EXTRA_STATE"
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val inf = inflater.inflate(R.layout.fragment_favourite, container, false)

        return inf

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_favourites.layoutManager = LinearLayoutManager(requireContext())
        rv_favourites.setHasFixedSize(true)
        adapter = FavouriteAdapter(requireActivity())
        rv_favourites.adapter = adapter
//        fab_add.setOnClickListener {
//            val intent = Intent(getActivity(), FavouriteAddUpdateActivity::class.java)
//            startActivityForResult(intent, FavouriteAddUpdateActivity.REQUEST_ADD)
//        }

        favouriteHelper = FavouriteHelper.getInstance(requireContext())
        favouriteHelper.open()

        /*
        Cek jika savedInstaceState null makan akan melakukan proses asynctask nya
        jika tidak,akan mengambil arraylist nya dari yang sudah di simpan
         */
        if (savedInstanceState == null) {
            loadFavouritesAsync()
        } else {
            val list = savedInstanceState.getParcelableArrayList<Favourite>(EXTRA_STATE)
            if (list != null) {
                adapter.listFavourites = list
            }
        }
    }

    private fun loadFavouritesAsync() {
        GlobalScope.launch(Dispatchers.Main) {
            progressbar.visibility = View.VISIBLE
            val deferredFavourites = async(Dispatchers.IO) {
                favouriteHelper.open()
                val cursor = favouriteHelper.queryAll()
                MappingHelper.mapCursorToArrayList(cursor)
            }
            progressbar.visibility = View.INVISIBLE
            val favourites = deferredFavourites.await()
            if (favourites.size > 0) {
                adapter.listFavourites = favourites
            } else {
                adapter.listFavourites = ArrayList()
                showSnackbarMessage("Tidak ada data saat ini")
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(EXTRA_STATE, adapter.listFavourites)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null) {
            when (requestCode) {
                // Akan dipanggil jika request codenya ADD
                FavouriteAddUpdateActivity.REQUEST_ADD -> if (resultCode == FavouriteAddUpdateActivity.RESULT_ADD) {
                    val favourite = data.getParcelableExtra<Favourite>(FavouriteAddUpdateActivity.EXTRA_FAVOURITE)

                    adapter.addItem(favourite)
                    rv_favourites.smoothScrollToPosition(adapter.itemCount - 1)

                    showSnackbarMessage("Satu item berhasil ditambahkan")
                }
                // Update dan Delete memiliki request code sama akan tetapi result codenya berbeda
                FavouriteAddUpdateActivity.REQUEST_UPDATE ->
                    when (resultCode) {
                        /*
                        Akan dipanggil jika result codenya  UPDATE
                        Semua data di load kembali dari awal
                        */
                        FavouriteAddUpdateActivity.RESULT_UPDATE -> {

                            val favourite = data.getParcelableExtra<Favourite>(FavouriteAddUpdateActivity.EXTRA_FAVOURITE)
                            val position = data.getIntExtra(FavouriteAddUpdateActivity.EXTRA_POSITION, 0)

                            adapter.updateItem(position, favourite)
                            rv_favourites.smoothScrollToPosition(position)

                            showSnackbarMessage("Satu item berhasil diubah")
                        }
                        /*
                        Akan dipanggil jika result codenya DELETE
                        Delete akan menghapus data dari list berdasarkan dari position
                        */
                        FavouriteAddUpdateActivity.RESULT_DELETE -> {
                            val position = data.getIntExtra(FavouriteAddUpdateActivity.EXTRA_POSITION, 0)

                            adapter.removeItem(position)

                            showSnackbarMessage("Satu item berhasil dihapus")
                        }
                    }
            }
        }
    }

//    override fun onDestroy() {
//        super.onDestroy()
//        favouriteHelper.close()
//    }

    /**
     * Tampilkan snackbar
     *
     * @param message inputan message
     */
    private fun showSnackbarMessage(message: String) {
        Snackbar.make(rv_favourites, message, Snackbar.LENGTH_SHORT).show()
    }
}
