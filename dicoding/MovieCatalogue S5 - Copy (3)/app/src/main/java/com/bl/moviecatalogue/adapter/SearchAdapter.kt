package com.bl.moviecatalogue.adapter

import android.content.ContentValues
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bl.moviecatalogue.R
import com.bl.moviecatalogue.Tv
import com.bl.moviecatalogue.db.DatabaseContract
import com.bl.moviecatalogue.db.DatabaseHelper
import com.bl.moviecatalogue.db.FavouriteHelper
import com.bl.moviecatalogue.helper.MappingHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_tv.view.*

class SearchAdapter : RecyclerView.Adapter<SearchAdapter.ListViewHolder>() {
    private lateinit var favouriteHelper: FavouriteHelper

    private val mData = ArrayList<Tv>()
    fun setData(items: ArrayList<Tv>) {
        mData.clear()
        mData.addAll(items)
        notifyDataSetChanged()
    }

    private var onItemClickCallback: OnItemClickCallback? = null
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_tv, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val context = holder.itemView.context
        favouriteHelper = FavouriteHelper.getInstance(context)
        favouriteHelper.open()
        holder.bind(mData[position])
        holder.btnFavorite.setOnClickListener {
            val id = mData[position].id.toString().trim()
            val title = mData[position].tv_name.toString().trim()
            val date = mData[position].tv_date.toString().trim()
            val description = mData[position].tv_description.toString().trim()
            val photo = mData[position].tv_photo.toString().trim()
            val values = ContentValues()
            values.put(DatabaseContract.FavouriteColumns._ID, id)
            values.put(DatabaseContract.FavouriteColumns.TITLE, title)
            values.put(DatabaseContract.FavouriteColumns.DATE, date)
            values.put(DatabaseContract.FavouriteColumns.DESCRIPTION, description)
            values.put(DatabaseContract.FavouriteColumns.PHOTO,"https://image.tmdb.org/t/p/w185"+photo)
            val result = favouriteHelper.insert(values)

            if (result > 0) {
                Toast.makeText(holder.itemView.context, "Favorite " + mData[position].tv_name, Toast.LENGTH_SHORT).show()
                val cursor = favouriteHelper.queryAll()
                MappingHelper.mapCursorToArrayList(cursor)
            } else {
                Toast.makeText(holder.itemView.context, "Movies/TV Show has on the list", Toast.LENGTH_SHORT).show()
            }



        }

    }

    override fun getItemCount(): Int = mData.size

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var btnFavorite: Button = itemView.findViewById(R.id.fav_button)
        fun bind(tvItems: Tv) {
            val urlmovie = "https://image.tmdb.org/t/p/w185/${tvItems.tv_photo}"

            with(itemView) {
                Glide.with(itemView.context)
                    .load(urlmovie)
                    .apply(RequestOptions().override(150, 150))
                    .into(tv_item_photo)
                tv_item_name.text = tvItems.tv_name
                tv_item_date.text = tvItems.tv_date
                tv_item_description.text = tvItems.tv_description

                itemView.setOnClickListener { onItemClickCallback?.onItemClicked(tvItems) }
            }
        }
    }
    interface OnItemClickCallback {
        fun onItemClicked(data: Tv)
    }
}