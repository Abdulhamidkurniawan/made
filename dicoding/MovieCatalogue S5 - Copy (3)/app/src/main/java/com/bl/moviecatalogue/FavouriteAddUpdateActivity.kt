package com.bl.moviecatalogue

import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bl.moviecatalogue.db.DatabaseContract
import com.bl.moviecatalogue.db.FavouriteHelper
import com.bl.moviecatalogue.entity.Favourite
import kotlinx.android.synthetic.main.activity_favourite_add_update.*
import java.text.SimpleDateFormat
import java.util.*

class FavouriteAddUpdateActivity : AppCompatActivity(), View.OnClickListener {
    private var isEdit = false
    private var favourite: Favourite? = null
    private var position: Int = 0
    private lateinit var favouriteHelper: FavouriteHelper

    companion object {
        const val EXTRA_FAVOURITE = "extra_favourite"
        const val EXTRA_POSITION = "extra_position"
        const val REQUEST_ADD = 100
        const val RESULT_ADD = 101
        const val REQUEST_UPDATE = 200
        const val RESULT_UPDATE = 201
        const val RESULT_DELETE = 301
        const val ALERT_DIALOG_CLOSE = 10
        const val ALERT_DIALOG_DELETE = 20
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_add_update)

        favouriteHelper = FavouriteHelper.getInstance(applicationContext)

        favourite = intent.getParcelableExtra(EXTRA_FAVOURITE)
        if (favourite != null) {
            position = intent.getIntExtra(EXTRA_POSITION, 0)
            isEdit = true
        } else {
            favourite = Favourite()
        }

        val actionBarTitle: String
        val btnTitle: String

        if (isEdit) {
            actionBarTitle = "Ubah"
            btnTitle = "Update"

            favourite?.let { edt_title.setText(it.title) }
            favourite?.let { edt_description.setText(it.description) }

        } else {
            actionBarTitle = "Tambah"
            btnTitle = "Simpan"
        }

        supportActionBar?.title = actionBarTitle
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_submit.text = btnTitle

        btn_submit.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        if (view.id == R.id.btn_submit) {
            val title = edt_title.text.toString().trim()
            val description = edt_description.text.toString().trim()

            /*
            Jika fieldnya masih kosong maka tampilkan error
             */
            if (title.isEmpty()) {
                edt_title.error = "Field can not be blank"
                return
            }

            favourite?.title = title
            favourite?.description = description

            val intent = Intent()
            intent.putExtra(EXTRA_FAVOURITE, favourite)
            intent.putExtra(EXTRA_POSITION, position)

            // Gunakan contentvalues untuk menampung data
            val values = ContentValues()
            values.put(DatabaseContract.FavouriteColumns.TITLE, title)
            values.put(DatabaseContract.FavouriteColumns.PHOTO, title)
            values.put(DatabaseContract.FavouriteColumns.DESCRIPTION, description)

            /*
            Jika merupakan edit maka setresultnya UPDATE, dan jika bukan maka setresultnya ADD
            */
            if (isEdit) {
                val result = favouriteHelper.update(favourite?.id.toString(), values)
                if (result > 0) {
                    setResult(RESULT_UPDATE, intent)
                    finish()
                } else {
                    Toast.makeText(this@FavouriteAddUpdateActivity, "Gagal mengupdate data", Toast.LENGTH_SHORT).show()
                }
            } else {
                favourite?.date = getCurrentDate()
                values.put(DatabaseContract.FavouriteColumns.DATE, getCurrentDate())
                val result = favouriteHelper.insert(values)

                if (result > 0) {
                    favourite?.id = result.toInt()
                    setResult(RESULT_ADD, intent)
                    finish()
                } else {
                    Toast.makeText(this@FavouriteAddUpdateActivity, "Gagal menambah data", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getCurrentDate(): String {
        val dateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault())
        val date = Date()

        return dateFormat.format(date)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (isEdit) {
            menuInflater.inflate(R.menu.menu_form, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.action_delete -> showAlertDialog(ALERT_DIALOG_DELETE)
//            android.R.id.home -> showAlertDialog(ALERT_DIALOG_CLOSE)
//        }
//        return super.onOptionsItemSelected(item)
//    }

    override fun onBackPressed() {
        showAlertDialog(ALERT_DIALOG_CLOSE)
    }


    /*
    Konfirmasi dialog sebelum proses batal atau hapus
    close = 10
    deleteFavourite = 20
     */
    private fun showAlertDialog(type: Int) {
        val isDialogClose = type == ALERT_DIALOG_CLOSE
        val dialogTitle: String
        val dialogMessage: String

        if (isDialogClose) {
            dialogTitle = "Batal"
            dialogMessage = "Apakah anda ingin membatalkan perubahan pada form?"
        } else {
            dialogMessage = "Apakah anda yakin ingin menghapus item ini?"
            dialogTitle = "Hapus Favourite"
        }

        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(dialogTitle)
        alertDialogBuilder
            .setMessage(dialogMessage)
            .setCancelable(false)
            .setPositiveButton("Ya") { dialog, id ->
                if (isDialogClose) {
                    finish()
                } else {
                    val result = favouriteHelper.deleteById(favourite?.id.toString()).toLong()
                    if (result > 0) {
                        val intent = Intent()
                        intent.putExtra(EXTRA_POSITION, position)
                        setResult(RESULT_DELETE, intent)
                        finish()
                    } else {
                        Toast.makeText(this@FavouriteAddUpdateActivity, "Gagal menghapus data", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            .setNegativeButton("Tidak") { dialog, id -> dialog.cancel() }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }
}
